use rand::Rng;
use std::time::Duration;
use cpu_time::ProcessTime;

fn main() {
    const SIZE: usize = 10;
    // array of zeroes of size SIZE
    let mut array = [0; SIZE];
    for x in 0..array.len(){
        let rand:i32 = rand::thread_rng().gen_range(0..SIZE as i32);
        array[x] = rand;
    }
    // let mut array = [0; SIZE];
    // for x in 0..array.len(){
    //     //let rand:i32 = rand::thread_rng().gen_range(0..SIZE as i32);
    //     array[x] = x as i32;
    // }

    println!("Starting array: ");
    for x in 0..array.len(){
        println!("{}: {}", x, array[x]);
    }

    let start = ProcessTime::now();
    quicksort(&mut array);

    let cpu_time: Duration = start.elapsed();

    println!("Sorted array: ");
    for x in 0..array.len(){
        println!("{}: {}", x, array[x]);
    }

    println!("{:?}", cpu_time);
}

fn quicksort(slice: &mut [i32]) { // O(logn)
    if !slice.is_empty() { //jezeli podana tablica nie jest pusta
        let pivot = split(slice);   //pivot to wartosc funkcji newpivot
        let len = slice.len();

        quicksort(&mut slice[0..pivot]);    // wywolanie dla lewej czesci tablicy
        quicksort(&mut slice[pivot + 1..len]); //wywolanie dla prawej czesci tablicy
    }
}

fn split(slice: &mut [i32]) -> usize { // O(n)
    let len = slice.len();
    //jako pivot wybieramy ostatni element
    let pivot = slice[len - 1]; //wybor pivota jako
    let mut i = 0;
    let mut j = i;

    while j < len - 1 { //do konca tablicy
        if slice[j] < pivot { //jezeli znajdziemy element mniejszy od pivota
            slice.swap(i, j); //zamiana
            i += 1;
        }
        j += 1;
    }

    slice.swap(i, len - 1);

    return i;
}
