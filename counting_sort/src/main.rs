use rand::Rng;
use std::time::Duration;
use cpu_time::ProcessTime;

fn main() {
    const SIZE: usize = 100000;
    // array of zeroes of size 100
    let mut array = [0; SIZE];
    for x in 0..array.len(){
        let rand: i32 = rand::thread_rng().gen_range(0..SIZE).try_into().unwrap();
        array[x] = rand;
    }

    // println!("Starting array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    // count ma size SIZE bo to najwieksza liczba jaka moze wystapic (ogolnie trzeba znalezc maxa)
    let mut count = [0; SIZE];

    let mut output = [0; SIZE];

    let start = ProcessTime::now(); ///////////////////////////////////// CZAS

    // wartosci pierwotnego arraya sa inkrementowane w odpowiednich indeksach tablicy zliczajacej
    for i in 0..array.len(){
        count[array[i] as usize] += 1;
    }

    // zliczanie ile jest liczb mniejszych rownych danemu indeksowi
    for i in 1..count.len(){
        count[i] += count[i-1];
    }

    // szukanie indeksu dla kazdego elementu oryginalnej tablicy i umiejscowienie elementow w tablicy wyjsciowej
    for i in (0..(count.len())).rev() {
        output[count[array[i] as usize] - 1 as usize] = array[i];
        count[array[i] as usize] -= 1;
    }

    //skopiowanie wyjsciowej tablicy do oryginalnej
    for i in 0..output.len() {
        array[i] = output[i];
    }

    let cpu_time: Duration = start.elapsed(); /////////////////////////// CZAS

    // println!("count: ");
    // for i in 0..count.len(){
    //     println!("{}: {}", i, count[i]);
    // }

    // println!("Sorted output: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, output[x]);
    // }

    // println!("Sorted array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    println!("{:?}", cpu_time);
}
