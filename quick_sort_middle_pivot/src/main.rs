use rand::Rng;
use std::time::Duration;
use cpu_time::ProcessTime;

fn main() {
    const SIZE: usize = 22500;
    //array of zeroes of size SIZE
    let mut array = [0; SIZE];
    for x in 0..array.len(){
        let rand:i32 = rand::thread_rng().gen_range(1..(SIZE)as i32 /100 as i32);
        array[x] = rand;
    }
    // let mut array = [0; SIZE];
    // for x in 0..array.len(){
    //     //let rand:i32 = rand::thread_rng().gen_range(0..SIZE as i32);
    //     array[x] = x as i32;
    // }

    // println!("Starting array: ");
    // for x in 0..100{
    //     println!("{}: {}", x, array[x]);
    // }

    let start = ProcessTime::now();
    quicksort(&mut array);
    let cpu_time: Duration = start.elapsed();

    // println!("Sorted array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    println!("{:?}", cpu_time);
}

fn quicksort(slice: &mut [i32]) {
    if !slice.is_empty() {
        let pivot = partition(slice);
        let len = slice.len();

        quicksort(&mut slice[0..pivot]);
        quicksort(&mut slice[pivot + 1..len]);
    }
}

fn partition(slice: &mut [i32]) -> usize {
    let len = slice.len();
    //jako pivot wybieramy ostatni element
    let pivot = slice[len/2];
    let mut i = 0;
    let mut j = 0;

    while j < len - 1 {
        if slice[j] <= pivot {
            slice.swap(i, j);
            i += 1;
        }
        j += 1;
    }

    slice.swap(i, len - 1);

    return i;
}
