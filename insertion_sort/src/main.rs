//use rand::Rng;
use std::time::Duration;
use cpu_time::ProcessTime;

fn main() {
    const SIZE: usize = 10000;
    // array of zeroes of size SIZE
    // let mut array = [0; SIZE];
    // for x in 0..array.len(){
    //     let rand:i32 = rand::thread_rng().gen_range(0..SIZE as i32);
    //     array[x] = rand;
    // }
    // let mut array = [0; SIZE]; //WORKS
    // for x in 0..array.len(){
    //     //let rand:i32 = rand::thread_rng().gen_range(0..SIZE as i32);
    //     array[x] = x as i32;
    // }
    let mut array = [0; SIZE];
    for x in array.len()-1..0{
        //let rand:i32 = rand::thread_rng().gen_range(0..SIZE as i32);
        array[x] = x as i32;
    }j

    // println!("Starting array: ");
    // for x in 0..arr.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    let start = ProcessTime::now();

    for i in 1..array.len(){
        let mut j = i;
        //dopoki obecny element jest mniejszy od poprzedzajacego - swap
        while j > 0 && array[j] < array[j-1]{
            array.swap(j-1, j);
            j = j-1;
        }
    }

    let cpu_time: Duration = start.elapsed();

    // println!("Sorted array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    println!("{:?}", cpu_time);
}
