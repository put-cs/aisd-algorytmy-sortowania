use rand::Rng;
use std::time::Duration;
use cpu_time::ProcessTime;

fn main() {
    const SIZE: usize = 100000;
    // array of zeroes of size SIZE
    let mut array = [0; SIZE];
    for x in 0..array.len(){
        let rand:i32 = rand::thread_rng().gen_range(0..SIZE as i32);
        array[x] = rand;
    }

    // println!("Starting array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    let mut min_index;
    let start = ProcessTime::now();
    for i in 0..array.len(){
        min_index = i;
        for j in (i+1)..array.len(){
            if array[j]<array[min_index] {
                min_index = j;
            }
        }
        array.swap(i, min_index);
    }
    let cpu_time: Duration = start.elapsed();

    // println!("Sorted array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    println!("{:?}", cpu_time);
}
