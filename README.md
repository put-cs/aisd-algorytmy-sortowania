# AiSD - Algorytmy Sortowania

Implementacja algorytmów sortowania w języku Rust. Do sprawozdania na Algorytmy i Struktury Danych.

## Autorzy
Michał Miłek

Sebastian Nowak

## Licencja
Projekt opublikowany jest na licencji GNU GPLv3
