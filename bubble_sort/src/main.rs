use rand::Rng;
use std::time::Duration;
use cpu_time::ProcessTime;

fn main() {
    const SIZE: usize = 100000;
    // array of zeroes of size SIZE
    let mut array = [0; SIZE];
    for x in 0..array.len(){
        let rand:i32 = rand::thread_rng().gen_range(0..SIZE as i32);
        array[x] = rand;
    }

    // println!("Starting array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    let start = ProcessTime::now();

    // podwojna petla w ktorej w kazdej iteracji najmniejsza wartosc jest zamieniana po kolei az do poczatku tablicy
    for i in 0..array.len() -1 {
        for j in 0..array.len() - i -1 {
            if array[j] > array[j+1]{
                array.swap(j, j+1);
            }
        }
    }
    let cpu_time: Duration = start.elapsed();

    // println!("Sorted array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    println!("{:?}", cpu_time);
}
