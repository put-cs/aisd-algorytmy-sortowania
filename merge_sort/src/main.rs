use rand::Rng;
use std::time::Duration;
use cpu_time::ProcessTime;

fn main() {
    const SIZE: usize = 100000;
    // array of zeroes of size SIZE
    let mut array = [0; SIZE];
    for x in 0..array.len(){
        let rand:i32 = rand::thread_rng().gen_range(0..SIZE as i32);
        array[x] = rand;
    }

    // println!("Starting array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    let start = ProcessTime::now();
    mergesort(&mut array);
    let cpu_time: Duration = start.elapsed();

    // println!("Sorted array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    println!("{:?}", cpu_time);
}

fn mergesort(array: &mut [i32]){

    let mid = array.len()/2; // wybieramy srodkowy element

    if array.len() < 2{ // nie ma sensu tego sortowac
        return;
    }

    let mut sorted = array.to_vec(); //

    mergesort(&mut array[..mid]); // wywolujemy funkcje rekurencyjnie dla lewej i prawej czesci tablicy
    mergesort(&mut array[mid..]);

    merge(&array[..mid], &array[mid..], &mut sorted);
    array.copy_from_slice(&sorted); //kopiuj sorted DO array
}

fn merge(array_left: &[i32], array_right: &[i32], sorted: &mut [i32]){
    let left_len = array_left.len();
    let right_len = array_right.len();
    let mut left = 0;
    let mut right = 0;
    let mut i = 0; //liczba wpisanych do arraya wynikowego elementow

    while left < left_len && right < right_len{ //iterujemy przez wszystkie elementy obu tablic

        if array_left[left] <= array_right[right]{ // wybieramy mniejszy element z pary
            sorted[i] = array_left[left]; // przepisujemy go do tablicy wynikowej
            left += 1;  //przechodzimy do kolejnego elementu
            i += 1; // iterujemy ilosc wpisanych elementow
        }

        else{
            sorted[i] = array_right[right]; // jezeli prawy wiekszy, przepisujemy prawy
            right += 1;
            i += 1;
        }

    }

    if left < left_len { // jezeli zostaly jakies nieprzepisane elementy, kopiujemy je
        sorted[i..].copy_from_slice(&array_left[left..]);
    }
    if right < right_len {
        sorted[i..].copy_from_slice(&array_right[right..]);
    }
}

