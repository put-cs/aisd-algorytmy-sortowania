use rand::Rng;
use std::time::Duration;
use cpu_time::ProcessTime;

fn main() {
    const SIZE: usize = 100000;
    // array of zeroes of size SIZE
    let mut array = [0; SIZE];
    for x in 0..array.len(){
        let rand:i32 = rand::thread_rng().gen_range(0..SIZE as i32);
        array[x] = rand;
    }

    // println!("Starting array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    let start = ProcessTime::now();  //////////////////////////

    // Stworzenie z losowo wygenerowanej tablicy kopca
    let end = array.len();
    for start in (0..end / 2).rev() {
        make_heap(&mut array, start, end - 1);
    }

    // Zamiana ostatniego elementu z pierwszym z kopca i odbudowanie kopca
    for end in (1..array.len()).rev() {
        array.swap(end, 0);
        make_heap(&mut array, 0, end - 1);
    }

    let cpu_time: Duration = start.elapsed(); ///////////////

    // println!("Sorted array: ");
    // for x in 0..array.len(){
    //     println!("{}: {}", x, array[x]);
    // }

    println!("{:?}", cpu_time);
}

// funkcja przesiewajaca
fn make_heap(array: &mut [i32], start: usize, end: usize) {
    let mut root = start;
    loop {
        // Indeks lewego dziecka
        let mut child = root * 2 + 1;
        if child > end {
            break;
        }
        // Prawe dziecko istnieje i jest wieksze
        if child < end && array[child] < array[child + 1] {
            child += 1;
        }

        if array[root] < array[child] {
            // Zamiana dziecka z rodzicem jesli dziecko jest wieksze od roota
            array.swap(root, child);
            root = child;
        } else {
            break;
        }
    }
}
